export default ({ store, redirect }) => {
  if (store.state.currentUser == null) {
    redirect('/client/auth/login')
  }
}
