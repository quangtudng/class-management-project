export default ({ store, redirect }) => {
  try {
    if (
      store.state.currentUser.roles.Name == 'Student' ||
      store.state.currentUser.roles.Name == 'Teacher'
    ) {
      store.dispatch('logout')
      redirect('/admin/auth/login')
    }
  } catch (error) {
    store.dispatch('logout')
    redirect('/admin/auth/login')
  }
}
