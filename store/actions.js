const Cookie = require('js-cookie')
const CookieParser = require('cookieparser')
export default {
  async nuxtServerInit({ commit }, { req }) {
    let user = null
    if (req.headers.cookie) {
      const parsed = CookieParser.parse(req.headers.cookie)
      try {
        const token = parsed.token
        const response = await this.$axios.get('/user/me', {
          headers: { Authorization: `Bearer ${token}` }
        })
        user = response.data.data
      } catch (error) {
        // No valid cookie found
      }
    }
    commit('SET_USER', user)
  },
  async login({ commit }, payload) {
    // Login API
    const response = await this.$axios.post('/login', payload)
    if (response.data.status == 200) {
      // Get current user using token
      const user = await this.$axios.get('/user/me', {
        headers: { Authorization: `Bearer ${response.data.token}` }
      })
      // Set token into cookie and set user into store
      Cookie.set('token', response.data.token, { expires: 1 / 12 })
      commit('SET_USER', user.data.data)
    } else {
      throw response.data
    }
  },
  logout({ commit }) {
    Cookie.remove('token')
    commit('SET_USER', null)
  }
}
