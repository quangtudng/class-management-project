import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate'
import { required, email } from 'vee-validate/dist/rules'
extend('required', {
  ...required,
  message: 'error.requiredField'
})
extend('email', {
  ...email,
  message: 'error.emailValid'
})
extend('length', {
  validate(value) {
    return value.length >= 6
  },
  message: 'error.lengthGreaterThan6'
})
Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)
